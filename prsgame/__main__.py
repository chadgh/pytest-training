# prsgame/__main__.py
import sys
import random

from .logic import determine_winner

CHOICES = list('prs')


def main(rounds):
    for round_num in range(rounds):
        print(f'Round {round_num + 1}:')
        choice = input('[p]aper, [r]ock, or [s]cissors: ')

        computer_choice = random.choice(CHOICES)

        print(f'The computer played: {computer_choice}')

        winner = determine_winner(choice, computer_choice)

        result = 'You win!' if winner == 'player1' else 'You lose.'
        result = 'Tie.' if winner == 'tie' else result

        print(f'The result is: {result}')


if __name__ == '__main__':
    main(int(sys.argv[1]))

