def determine_winner(player1_choice, player2_choice):
    possible_games = {
        'pp': 'tie',
        'pr': 'player1',
        'ps': 'player2',
        'rp': 'player2',
        'rr': 'tie',
        'rs': 'player1',
        'sp': 'player1',
        'sr': 'player2',
        'ss': 'tie',
    }
    return possible_games[f'{player1_choice}{player2_choice}']

